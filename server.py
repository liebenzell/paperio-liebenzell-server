import asyncio
import websockets
import json
import logging
from game import Game
from player import Player
from settings import PORT, IP, DEBUG_LEVEL

logging.getLogger().setLevel(DEBUG_LEVEL)


class Server:

    def __init__(self):
        self.game = Game(self)

        start_server = websockets.serve(self.listen, IP, PORT)

        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()

    async def listen(self, websocket, path):
        try:
            async for message in websocket:
                try:
                    data = json.loads(message)
                except json.JSONDecodeError as e:
                    msg = "JSONDecodeError: Message received was not JSON-encoded"
                    logging.error(msg)
                    logging.info(e)
                    await websocket.send(json.dumps({"event": "error", "message": msg}))
                    continue

                if "event" not in data:
                    msg = "Received message has not the key 'event', but it's necessary."
                    logging.error(msg)
                    await websocket.send(json.dumps({"event": "error", "message": msg}))
                    continue
                elif data["event"] == "watch":
                    await self.game.add_watcher(websocket)
                elif data["event"] == "connect":
                    await self.game.add_player(websocket, Player(data.get("name")))
                elif data["event"] == "go_direction":
                    if not self.game.players.get(websocket):
                        msg = "Received sth form dead player"
                        logging.error(msg)
                        await websocket.send(json.dumps({"event": "error", "message": msg}))
                    elif self.game.players.get(websocket).direction in ["up", "down"]\
                            and data.get("direction") in ["left", "right"] \
                            or self.game.players.get(websocket).direction in ["left", "right"]\
                            and data.get("direction") in ["up", "down"]:
                        self.game.players.get(websocket).direction = data.get("direction")
                    elif data.get("direction") not in ["left", "right", "up", "down"]:
                        await websocket.send(json.dumps({"event": "error", "message": "invalid direction"}))
                else:
                    msg = f"Unsupported event: {data['event']}"
                    logging.error(msg)
                    await websocket.send(json.dumps({"event": "error", "message": msg}))
        except websockets.exceptions.ConnectionClosedError:
            print("connection closed")
        finally:
            self.game.remove_player(websocket)

    async def send_to_all(self, message):
        if self.game.players:  # asyncio.wait doesn't accept an empty list
            await asyncio.wait([user.send(message) for user in self.game.players])
        if self.game.watchers:
            await asyncio.wait([user.send(message) for user in self.game.watchers])

    async def send_welcome(self, websocket, player):
        await websocket.send(self.game.get_welcome_message(player))


if __name__ == '__main__':
    Server()
