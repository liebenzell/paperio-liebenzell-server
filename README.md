# paperio-liebenzell-server

## Einleitung
Der BWINF-Bieber liebt Videospiele. Sein Lieblingsspiel [paper.io](http://paper.io)
kann er im Browser gegen Spieler aus aller Welt spielen. 
Allerdings ist er sehr vorsichtig geworden, was das Ausführen von fremdem 
Javascript-Code in seinem Browser angeht. 
Deshalb hat er jetzt jegliche Ausführung von Javascript blockiert. 
Leider funktioniert paper.io dadurch nicht mehr. 
Deshalb hat er mich gefragt, ob ich das Spiel nochmal für ihn implementieren kann. 
Bis jetzt habe ich aber erst einen Server programmiert, der das Spielfeld für jeden Schritt berechnet.
Mit dem Server alleine ist noch kein Spiel möglich, es wird dazu noch ein Client gebraucht, der das
Spielfeld darstellt und die Spielzüge des Spielers an den Server sendet.

Könntet ihr mir helfen einen Client zu schreiben, damit wir am Ende zusammen spielen können?
Ihr könnt jede Programmiersprache verwenden, die ihr drauf habt. 
Mit dem Server, dessen Python-Code hier im Repository ist, 
wird über WebSocket per JSON kommuniziert. 
Ihr werdet also im Laufe des Projekts mit Bibliotheken für WebSocket, JSON 
und einem GUI zu tun haben.


## Schnittstelle

Für die Kommunikation muss der Client eine WebSocket-Verbindung zum Server öffnen.
Wenn ihr den Server Code lokal bei euch ausführt, lauten die Daten so:

| Domain                 | Port |
| ---------------------- | ---- |
| localhost | 1893 |

Damit der Client auf das Spielfeld kommt, muss er folgende Nachricht an den Server senden:

```json
{
    "event": "connect"
}
```

Daraufhin sendet der Server eine "welcome"-Nachricht mit der Konfiguration an den Client:

```json
{
  "event": "welcome",
  "config": {
    "field_width": 10,
    "field_height": 9,
    "time_step": 0.2
  },
  "player_id": 2,
  "players": [
    {
      "id": 1,
      "name": "Player 1",
      "color": "#282d1d",
      "direction": "up",
      "position": [2, 1],
      "new_tail": true
    },
    {
      "id": 2,
      "name": "Player 2",
      "color": "#0d4409",
      "direction": "down",
      "position": [3, 6],
      "new_tail": false
    }
  ],
  "ownership": [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 2, 2, 2, 0],
    [0, 0, 0, 0, 0, 2, 2, 2, 0],
    [0, 0, 0, 0, 0, 2, 2, 2, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 0, 0, 0, 0, 0, 0]
  ],
  "tails": [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0]
  ]
}
```

oder einen Fehler, wenn das Spielfeld schon voll ist:

```json
{
  "event": "error",
  "message": "no more space on field"
}
```
---

Ab jetzt gibt es vier mögliche Nachrichten, die der Server als Spielzüge annimmt:

```json
{"event": "go_direction", "direction": "up"}
```
```json
{"event": "go_direction", "direction": "right"}
```
```json
{"event": "go_direction", "direction": "down"}
```
```json
{"event": "go_direction", "direction": "left"}
```

Bei anderen Nachrichten, antwortet der Server mit einem Fehler:
```json
{"event": "error", "message": "JSONDecodeError: Message received was not JSON-encoded"}
```
```json
{"event": "error", "message": "Received message has not the key 'event', but it's necessary."}
```
```json
{"event": "error", "message": "Received sth form dead player"}
```
```json
{"event": "error", "message": "invalid direction"}
```
```json
{"event": "error", "message": "Unsupported event: ***"}
```

---
Jede `time_step` Sekunden, sendet der Server ein Update an alle Spieler:
```json
{
  "event": "update",
  "players": [
    {
      "id": 3,
      "name": "Player 3",
      "color": "#282d1d",
      "direction": "up",
      "position": [8, 1],
      "new_tail": true
    },
    {
      "id": 4,
      "name": "Player 4",
      "color": "#6c39db",
      "direction": "right",
      "position": [7, 1],
      "new_tail": false
    }
  ],
  "kills": [],
  "clean_owner": [],
  "clean_tail": [
    [6, 1],
    [6, 0]
  ],
  "new_ownership": {
    "4": [
      [6, 1],
      [6, 0]
    ]
  }
}
```
oder
```json
{
  "event": "update",
  "players": [
    {
      "id": 4,
      "name": "Player 4",
      "color": "#6c39db",
      "direction": "right",
      "position": [8, 1],
      "new_tail": false
    }
  ],
  "kills": [
    3
  ],
  "clean_owner": [
    [7, 6],
    [9, 8],
    [7, 7],
    [8, 8],
    [8, 7],
    [9, 6],
    [8, 6],
    [7, 8],
    [9, 7]
  ],
  "clean_tail": [
    [8, 3],
    [8, 2],
    [8, 1],
    [8, 0],
    [8, 5],
    [8, 4]
  ],
  "new_ownership": {
    
  }
}
```
	
## Installation

### Methode mit Virtual Environment (empfohlen)
1. Installiere Python 3.6 (mindestens)
2. Installiere das Paket für Virtual Environments (unter Ubuntu: `python3-venv` oder `python3.6-venv`)
3. Erzeuge das Virtual Environment
    ```bash
    python3 -m venv venv
    ```
4. Aktiviere das Virtual Environment
    ```bash
    source venv/bin/activate
    ```
5. Installiere die benötigten Bibliotheken
    ```bash
    pip3 install -r requirements.txt
    ```
6. Führe main.py mit Python aus
    ```bash
    python3 main.py
    ```

### Methode ohne Virtual Environment
1. Installiere Python 3.6 (mindestens)
2. Installiere die benötigten Bibliotheken mit 
    ```bash
    sudo pip3 install -r requirements.txt
    ```
3. Führe main.py mit Python aus, mit
    ```bash
    python3 main.py
    ```
