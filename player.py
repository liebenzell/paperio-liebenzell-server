import random
from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000


class Player:
    ID_COUNTER = 1
    USED_COLORS = {"#e0e0e0"}

    def __init__(self, name):
        self.id = Player.ID_COUNTER
        Player.ID_COUNTER += 1
        self.name = name if name else f"Player {self.id}"

        self.direction = random.choice(["left", "right", "up", "down"])

        self.pos_x = None
        self.pos_y = None

        def calc(color1, color2):
            color1_lab = convert_color(sRGBColor.new_from_rgb_hex(color1), LabColor)
            color2_lab = convert_color(sRGBColor.new_from_rgb_hex(color2), LabColor)
            return delta_e_cie2000(color1_lab, color2_lab)

        colors = ["#" + ("%06x" % random.randint(0, 0xFFFFFF)) for _ in range(20)]

        self.color = max(colors, key=lambda c: min(calc(uc, c) for uc in Player.USED_COLORS))
        Player.USED_COLORS.add(self.color)

        self.tail = []

    def __hash__(self):
        return self.id.__hash__()

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "name": self.name,
            "color": self.color,
            "direction": self.direction,
            "position": (int(self.pos_x), int(self.pos_y)),
            "new_tail": (len(self.tail) > 0),
        }

    def __del__(self):
        self.delete_color()

    def delete_color(self):
        if self.color in Player.USED_COLORS:
            Player.USED_COLORS.remove(self.color)
