import logging

# Network
IP = ""
PORT = 1893

# Game
TIME_STEP = 0.2

FIELD_WIDTH = 16*4
FIELD_HEIGHT = 9*4

# Debug
DEBUG_LEVEL = logging.INFO
