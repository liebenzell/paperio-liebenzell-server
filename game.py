import asyncio
import json
import logging
import random
from collections import deque

import numpy as np

from settings import FIELD_WIDTH, FIELD_HEIGHT, TIME_STEP


class Game:
    def __init__(self, server):
        self.new_owners = {}
        self.server = server
        self.ownership = np.zeros(shape=(FIELD_WIDTH, FIELD_HEIGHT))
        self.tails = np.zeros(shape=(FIELD_WIDTH, FIELD_HEIGHT))
        self.players = {}
        self.watchers = set()
        self.players_left = []

    async def add_watcher(self, websocket):
        self.watchers.add(websocket)
        await self.server.send_welcome(websocket, None)

    async def add_player(self, websocket, player):
        if not self.players:  # first player has connected
            asyncio.ensure_future(self.loop())

        if websocket in self.players:
            logging.error("you can only live once")
            await websocket.send(json.dumps({
                "event": "error",
                "message": "you can only live once"
            }))
            return False

        start_position = self.generate_start_position()

        if start_position:
            player.pos_x = start_position[0]
            player.pos_y = start_position[1]

            owner = [(int(start_position[0] + a), int(start_position[1] + b)) for a in [-1, 0, 1] for b in [-1, 0, 1]]
            for (x, y) in owner:
                self.ownership[x][y] = player.id

            await self.server.send_welcome(websocket, player)

            self.new_owners[player.id] = owner

            self.players[websocket] = player
            return True
        else:
            logging.error("No more space on field")
            await websocket.send(json.dumps({
                "event": "error",
                "message": "no more space on field"
            }))
            return False

    def remove_player(self, websocket):
        if websocket in self.players:
            self.players_left.append(self.players[websocket])
            del self.players[websocket]
        if websocket in self.watchers:
            self.watchers.remove(websocket)

    def get_welcome_message(self, player):
        return json.dumps({
            "event": "welcome",
            "config": {
                "field_width": FIELD_WIDTH,
                "field_height": FIELD_HEIGHT,
                "time_step": TIME_STEP
            },
            "player_id": player.id if player else -1,
            "players": [p.to_dict() for p in self.players.values()] + ([player.to_dict()] if player else []),
            "ownership": self.ownership.tolist(),
            "tails": self.tails.tolist(),
        })

    async def loop(self):
        while self.players:
            await asyncio.sleep(TIME_STEP)

            kills = set()
            for p in self.players_left:
                kills.add(p)
            self.players_left = []

            owner_diff = self.new_owners
            clean_owner = set()
            clean_tail = set()

            players = self.players.copy().values()
            for p in players:
                # move one step in current direction
                p.pos_x += {"left": -1, "right": 1, "up": 0, "down": 0}[p.direction]
                p.pos_y += {"left": 0, "right": 0, "up": -1, "down": 1}[p.direction]

                if p.pos_x in range(0, FIELD_WIDTH) and p.pos_y in range(0, FIELD_HEIGHT):
                    # player hasn't left the field

                    if self.tails[p.pos_x][p.pos_y] != 0:
                        # if player crashed into a tail, add the corresponding player to 'kills'
                        crashed = {p.id: p for p in self.players.values()}.get(self.tails[p.pos_x][p.pos_y])
                        if crashed:
                            kills.add(crashed)

                    if self.ownership[p.pos_x][p.pos_y] == p.id:
                        if p.tail:
                            # if player took over a new area
                            # add the enclosed cells to his ownership
                            new_cells = self.fill_tail(p)

                            # remove his tail
                            for x, y in p.tail:
                                clean_tail.add((int(x), int(y)))  # for clients to get diff for tails array
                                self.tails[x][y] = 0
                            p.tail = []

                            # if another player is enclosed by the new ownership, that player is game over
                            for player in self.players.values():
                                if player.id != p.id and (player.pos_x, player.pos_y) in new_cells:
                                    kills.add(player)

                            owner_diff[p.id] = list(new_cells)  # new ownership is added to diff list

                    else:
                        # if player is not on his ownership, his tail is extended to his new position
                        p.tail.append((p.pos_x, p.pos_y))
                        self.tails[p.pos_x][p.pos_y] = p.id

                else:  # if player has left the field
                    kills.add(p)

            for p in kills:
                p.delete_color()
                for x in range(FIELD_WIDTH):
                    for y in range(FIELD_HEIGHT):
                        # for clients make clean-up-sets with tuples
                        # so they can tidy up their ownership and tails arrays
                        if self.ownership[x][y] == p.id:
                            self.ownership[x][y] = 0
                            clean_owner.add((int(x), int(y)))
                        if self.tails[x][y] == p.id:
                            self.tails[x][y] = 0
                            clean_tail.add((int(x), int(y)))

                # remove killed player from dictionary
                websocket = [websocket for websocket in self.players if self.players[websocket] == p]
                if websocket:
                    del self.players[websocket[0]]
                    await websocket[0].send(json.dumps({"event": "game over"}))
                    # await socket.close()

            await self.server.send_to_all(json.dumps({
                "event": "update",
                "players": [p.to_dict() for p in self.players.values()],
                "kills": [p.id for p in kills],
                "clean_owner": list(clean_owner),
                "clean_tail": list(clean_tail),
                "new_ownership": owner_diff,
            }))
            self.new_owners = {}

    def generate_start_position(self):
        where = np.where((self.tails == 0) & (self.ownership == 0))

        valids = []
        for i in range(len(where[0])):
            x = where[0][i]
            y = where[1][i]
            if x not in range(1, FIELD_WIDTH - 1) or y not in range(1, FIELD_HEIGHT - 1):
                continue

            valid = True
            for a in [-1, 0, 1]:
                for b in [-1, 0, 1]:
                    if self.tails[x + a][y + b] != 0 or self.ownership[x + a][y + b] != 0:
                        valid = False
                    if not valid:
                        break
                if not valid:
                    break
            if valid:
                valids.append((x, y))
        if valids:
            return random.choice(valids)
        else:
            return None

    def fill_tail(self, p):
        new_cells = set()
        been = np.full(shape=(FIELD_WIDTH, FIELD_HEIGHT), fill_value=False)

        for r, c in p.tail:
            if been[r][c]:
                continue

            been[r][c] = True
            new_cells.add((int(r), int(c)))  # the tail is part of new ownership
            self.ownership[r][c] = p.id

            for x, y in [(r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1)]:
                if x not in range(FIELD_WIDTH) or y not in range(FIELD_HEIGHT) or been[x][y] \
                        or self.tails[x][y] == p.id or self.ownership[x][y] == p.id:
                    continue
                new_cells = new_cells.union(self.flood_fill(p, x, y, been))
        return new_cells

    def set_been(self, been, start, p):
        """
        This function surrounds the cells of the field (called obstacle) by True.
        :param been: Numpy array
        :param start: tuple with coordinates of start position
        :param p: Player object
        :return: None
        """

        def uniont(t1, t2) -> tuple:  # sum tuple pairwise
            return tuple(map(sum, zip(t1, t2)))

        # find a direction around the ownership/tail field
        for d in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
            x, y = uniont(start, d)  # calculate coordinates for temporary direction
            if x not in range(FIELD_WIDTH) or y not in range(FIELD_HEIGHT):
                continue
            if self.tails[x][y] != p.id and self.ownership[x][y] != p.id and not been[x][y]:
                # if cell is neither ownership nor tail, reset the coords to start and
                # take current 'd' as direction tuple
                x, y = start
                break

        # get position of obstacle in relation to start position
        obstacle_directions = {(1, 0): (0, 1), (-1, 0): (0, -1), (0, 1): (-1, 0), (0, -1): (1, 0)}
        obstacle_directions_inverse = {obstacle_directions[a]: a for a in obstacle_directions}
        obstacle_x, obstacle_y = uniont(start, obstacle_directions[d])
        if obstacle_x not in range(FIELD_WIDTH) or obstacle_y not in range(FIELD_HEIGHT) or \
                self.tails[obstacle_x][obstacle_y] != p.id and self.ownership[obstacle_x][obstacle_y] != p.id:
            # check whether a obstacle is on the right or left side of the start direction
            # if it's on the left side or the right side is out of field,
            # directions dictionaries are changed with each other
            obstacle_directions, obstacle_directions_inverse = obstacle_directions_inverse, obstacle_directions

        stop_helper = 0  # emergency exit of while loop if start coordinates are never reached
        while stop_helper < FIELD_HEIGHT * FIELD_WIDTH:

            # set current cell to True (if it's in the field's range)
            if x in range(FIELD_WIDTH) and y in range(FIELD_HEIGHT):
                been[x][y] = True

            # calc future coordinates based on current direction
            next_x, next_y = uniont((x, y), d)

            # if future coords are the beginning position, we are done
            if (next_x, next_y) == start:
                break

            # calc future obstacle cell position based on current direction and obstacle side
            obstacle_x, obstacle_y = uniont((next_x, next_y), obstacle_directions[d])

            # change direction if necessary
            if next_x not in range(FIELD_WIDTH) or next_y not in range(FIELD_HEIGHT):
                # go around the obstacle field even if the current position is not in range of the field.
                # there are three conditions that trigger a direction change:
                # - if current position is in field range and the next is out
                # - if future obstacle cell position is not in field range (edge of the field)
                # - if future obstacle cell is not an obstacle
                if x in range(FIELD_WIDTH) and y in range(FIELD_HEIGHT):
                    d = obstacle_directions[d]
                elif obstacle_x not in range(FIELD_WIDTH) or obstacle_y not in range(FIELD_HEIGHT):
                    d = obstacle_directions[d]
                elif self.tails[obstacle_x][obstacle_y] != p.id and self.ownership[obstacle_x][obstacle_y] != p.id:
                    d = obstacle_directions[d]

            elif self.tails[obstacle_x][obstacle_y] != p.id and self.ownership[obstacle_x][obstacle_y] != p.id:
                # if future obstacle cell is not an obstacle, change direction
                d = obstacle_directions[d]

            elif self.tails[next_x][next_y] == p.id or self.ownership[next_x][next_y] == p.id:
                # if future position is a obstacle cell, go in the opposite direction
                d = obstacle_directions_inverse[d]
                next_x, next_y = uniont((x, y), d)  # calc future position based on new direction

                # check if future position is in field range
                if next_x in range(FIELD_WIDTH) and next_y in range(FIELD_HEIGHT):

                    if self.tails[next_x][next_y] == p.id or self.ownership[next_x][next_y] == p.id:
                        # if new future position is an obstacle cell _too_, go in the
                        # opposite direction -> 180° in sum
                        d = obstacle_directions_inverse[d]
                        next_x, next_y = uniont((x, y), d)  # calc future position based on direction once again

                    # calc new obstacle cell position
                    obstacle_x, obstacle_y = uniont((next_x, next_y), obstacle_directions[d])

                    if self.tails[obstacle_x][obstacle_y] != p.id and self.ownership[obstacle_x][obstacle_y] != p.id:
                        # special case: change direction to obstacle direction, if obstacle isn't there
                        d = obstacle_directions[d]

                else:
                    # if not in field range, go around the obstacle field
                    d = obstacle_directions[d]

            # apply future coordinates
            x, y = next_x, next_y

            stop_helper += 1

    def flood_fill(self, p, r, c, been):
        start = (r, c)

        coords = deque()
        filled = set()
        surrounded = True

        coords.append(start)
        while len(coords) > 0:
            r, c = coords.pop()

            if r not in range(FIELD_WIDTH) or c not in range(FIELD_HEIGHT):
                surrounded = False
                break

            if been[r][c] or self.tails[r][c] == p.id or self.ownership[r][c] == p.id:
                continue

            been[r][c] = True
            filled.add((int(r), int(c)))

            for x, y in [(r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1)]:
                if x not in range(FIELD_WIDTH) or y not in range(FIELD_HEIGHT) or not been[x][y]:
                    coords.append((x, y))

        if surrounded:
            for r, c in filled:
                self.ownership[r][c] = p.id
            return filled
        else:
            for r, c in filled:
                been[r][c] = False
            self.set_been(been, start, p)
            return set()
